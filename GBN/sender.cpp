#include<iostream>
#include<fstream>
#include<time.h>
#include<unistd.h>
#include<cstring>
#include<string.h>
#include<cstdlib>
#include"unp.h"
using namespace std;
#define MYPORT 4321
struct rdt_pack
{
    char packet[985];
    int seq;
    uint16_t check_sum;
};

//########################################
int sockfd;
const int max_window=8;
int base=0;
int next_seq=0;
socklen_t length;
struct sockaddr_in servr_addr,myaddr2;
rdt_pack buffer[max_window];
unsigned int timeout_val=3;
const int MSS=985;
//########################################
uint16_t checkSum(void *buffer , size_t len);
int rdt_send(rdt_pack rdt_packet);
int random(int a)
{
srand(time(NULL)+a);
return (rand() % 10);
}


void timeOut (int sig)
{  
  signal(sig, timeOut);
  alarm(timeout_val);
  for(int i=base;i<next_seq;i++)
  {
  	rdt_send(buffer[i%max_window]); 
  }
}
ofstream log2("LogFile(GBN).txt");
char timearray[100];
//########################################

int main()
{
   	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	
         int k;
         k = fcntl(sockfd, F_GETFL, 0);
         k |= O_NONBLOCK;
         fcntl(sockfd, F_SETFL, k);
	servr_addr.sin_family = AF_INET;
	servr_addr.sin_port = htons(MYPORT); // short, network byte order
	servr_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	memset(&(servr_addr.sin_zero),'\0', 8); // zero the rest of the struct
	int b=sizeof(struct sockaddr);
	bind(sockfd,(struct sockaddr *)&servr_addr, b);
	int count=0;
	rdt_pack start;
         int a=-1;
         while(a<0)
         {
      a=recvfrom(sockfd, &start, sizeof(rdt_pack), 0,(struct sockaddr*)&myaddr2, &length);
      }
        perror("recv");
        cout<<start.packet<<endl;
        rdt_pack ackP;
       // ifstream fin("input.txt");
       ifstream fin("input.txt");
        int i=0;
        signal (SIGALRM, timeOut);
         time_t now = time(NULL);
tm * ptm = localtime(&now);
strftime(timearray, 100, "%d-%B-%G , %r", ptm);
	log2<<"Sender is started sending file at:      "<<timearray<<endl;
        while(!fin.eof() )
        {
 //               cout<<"in loop"<<endl;
                if(next_seq<(base+max_window))
                {
                buffer[i].seq=next_seq;
                char buf[MSS];
                fin.read(buf,MSS-1);//reading next packet
                 if(fin.gcount()<MSS-1)
                       {         
                           buf[fin.gcount()-1]='\0';
                       }
                       else
                        buf[MSS-1]='\0';
                strcpy(buffer[i].packet,buf);
                int rand=random(1333);
                if(rand!=5 && rand !=3)
                {
                buffer[i].check_sum=checkSum(buffer[i].packet,strlen(buffer[i].packet));
                }
                else
                {
                time_t now = time(NULL);
tm * ptm = localtime(&now);
strftime(timearray, 100, "%d-%B-%G , %r", ptm);
	log2<<"Inducing bit error at:                   "<<timearray<<endl;
                buffer[i].check_sum=33632;//inducing bit error
                }
                rdt_send(buffer[i]);//sending to rdt
                  
                  if(i==0)
                  {
                  alarm(timeout_val);
                  }
                        if(i==max_window-1)
                        {
                        i=0;
                        }
                        else
                        {
                        i++;
                        }
                  next_seq++;
                  count++;
                }
                a=recvfrom(sockfd, &ackP, sizeof(rdt_pack), 0,(struct sockaddr*)&myaddr2, &length);//receiving acks
               if(a>0)
               {
                        base=ackP.seq+1;
   //                     time_t now = time(NULL);
 ptm = localtime(&now);
strftime(timearray, 100, "%d-%B-%G , %r", ptm);
        log2<<"Moving base to "<<ackP.seq<<" sent at:             "<<timearray<<endl;
                        //cur_limit++;
      //                 time_t now = time(NULL);
 ptm = localtime(&now);
strftime(timearray, 100, "%d-%B-%G , %r", ptm);
       log2<<"Acknowledgment of packet:"<<ackP.seq<<" received at: "<<timearray<<endl;
                                 if(base==next_seq)
                        {
                                alarm(0);
                        }
                        else
                        alarm(timeout_val);
               }
        }
        while(1)
        {
        a=recvfrom(sockfd, &ackP, sizeof(rdt_pack), 0,(struct sockaddr*)&myaddr2, &length);
        if(a>0)
               {
                        base=ackP.seq+1;
                        //cur_limit++
                        if(base==next_seq)
                        {
                                
                                alarm(0);
                                break;
                        }
                        else
                        alarm(timeout_val);
               }
        }
        strcpy(buffer[0].packet,"end"); //sending exit signal to receiver
        rdt_send(buffer[0]);
        int s=close(sockfd);
        log2.close();
        fin.close();
         return 0;
}



int rdt_send(rdt_pack rdt_packet)
{
length=sizeof(myaddr2);
        //cout<<rdt_packet.packet<<endl;
        rdt_packet.check_sum=checkSum(rdt_packet.packet,strlen(rdt_packet.packet));
        int z=sendto(sockfd, &rdt_packet, sizeof(rdt_pack), 0,(struct sockaddr*)&myaddr2,length);
        time_t now = time(NULL);
tm * ptm = localtime(&now);
strftime(timearray, 100, "%d-%B-%G , %r", ptm);
        log2<<"Packet:"<<rdt_packet.seq<<" sent at:                       "<<timearray<<endl;
        return 0;
}

uint16_t checkSum(void *buffer , size_t len)
{
	const uint16_t * buff=(uint16_t*) buffer;
	
	uint32_t sum;
	sum=0;
	while(len>1)
	{
		sum+=*buff++;
		if(sum & 0x80000000)
		{
			sum = (sum & 0xFFFF) + (sum >> 16);
		}
		len-=2;
	}
	if(len & 1)
	{
		sum+=*((uint8_t *)buff);
	}
	while(sum>>16)
	{
		sum=(sum & 0xFFFF)+(sum>>16);
	}
	return ((uint16_t)(~sum));
}
