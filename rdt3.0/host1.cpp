#include<iostream>
#include<fstream>

#include<time.h>
#include<unistd.h>
#include<cstring>
#include<string.h>
#include<cstdlib>
#include"unp.h"
using namespace std;
#define MYPORT 1235
//////
struct rdt_pack
{
    char packet[985];
    int seq;
    uint16_t check_sum;
};
//########################################
uint16_t checkSum(void *buffer , size_t len);
int rdt_send(char packet[],int seqn);
int random(int a)
{
srand(time(NULL)+a);
return (rand() % 10);
}
//########################################
int sockfd,i=0;
const int max_window=4;
int base=0;
int next_seq=0;
socklen_t length;
struct sockaddr_in servr_addr,myaddr2;
char timearray[100];
const int MSS=985;
ofstream log("LogFile(S&W).txt");
//########################################
int main()
{


        int seq_no=1;
        char buf1[]="ACK0";
        char buf2[]="ACK1";
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	struct timeval timeout;      
        timeout.tv_sec = 4;
        timeout.tv_usec = 0;

        if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,sizeof(timeout)) < 0)
        {
              perror("setsockopt failed\n");
        }
	servr_addr.sin_family = AF_INET;
	servr_addr.sin_port = htons(MYPORT); // short, network byte order
	servr_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	memset(&(servr_addr.sin_zero),'\0', 8); // zero the rest of the struct
	int b=sizeof(struct sockaddr);
	bind(sockfd,(struct sockaddr *)&servr_addr, b);
	char buf[MSS],bu[100];
        length=sizeof(myaddr2);
        int a=recvfrom(sockfd, bu, 17, 0,(struct sockaddr*)&myaddr2, &length);
        rdt_pack ackP;
        time_t now = time(NULL);
tm * ptm = localtime(&now);
strftime(timearray, 100, "%d-%B-%G , %r", ptm);
        log<<"Sender is started sending file at:      "<<timearray<<endl;
        fstream fin("input.txt",ios::in);
        fin.read(buf,MSS-1);
        buf[MSS-1]='\0';
        while(!fin.eof())
        {
                rdt_send(buf,seq_no);//sending to rdt
                if(a<0)
                {
                        a=recvfrom(sockfd, &ackP, sizeof(rdt_pack), 0,(struct sockaddr*)&myaddr2, &length);// discarding the dropped ack
                }
 
                 a=recvfrom(sockfd, &ackP, sizeof(rdt_pack), 0,(struct sockaddr*)&myaddr2, &length);//receiving acks
                if(a<0)
                {
                        cout<<"Resending.."<<endl;
                        time_t now = time(NULL);
tm * ptm = localtime(&now);
strftime(timearray, 100, "%d-%B-%G , %r", ptm);
                        log<<"Packet:"<<seq_no<<" resent at:                     "<<timearray<<endl;
                        continue;//resending
                }
                if( (ackP.seq==seq_no && seq_no==0) || (ackP.seq==seq_no && seq_no==1) ) //succussful ac received
                {
                time_t now = time(NULL);
tm * ptm = localtime(&now);
strftime(timearray, 100, "%d-%B-%G , %r", ptm);
                log<<"Acknowledgment of packet:"<<ackP.seq<<" received at: "<<timearray<<endl;
                        if(seq_no==0)
                        {seq_no=1;}
                        else if(seq_no==1)
                        {seq_no=0;}
                        // cout<<fin.gcount()<<endl;
                        fin.read(buf,MSS-1);//reading next packet
                       if(fin.gcount()<MSS-1)
                       {
                               
                                buf[fin.gcount()-1]='\0';
                                rdt_send(buf,seq_no);
                       }
                       else
                        buf[MSS-1]='\0';
                        
                }
        }
        rdt_send("end",seq_no);
        int s=close(sockfd);
        fin.close();
         return 0;
}



int rdt_send(char packet[],int seqn)
{
        rdt_pack rdt_packet;
        int rand=random(1221);
        if(rand!=5 && rand!=7)
        {
        rdt_packet.check_sum=checkSum(packet,strlen(packet));
        }
        else
        {
        log<<"Induced bit error at:                  "<<timearray<<endl;
        rdt_packet.check_sum=98363;//inducing bit error
        }
        strcpy(rdt_packet.packet,packet);
        rdt_packet.seq=seqn;
        int z=sendto(sockfd, &rdt_packet, sizeof(rdt_pack), 0,(struct sockaddr*)&myaddr2,length);
        time_t now = time(NULL);
tm * ptm = localtime(&now);
strftime(timearray, 100, "%d-%B-%G , %r", ptm);
        log<<"Packet:"<<rdt_packet.seq<<" sent at:                       "<<timearray<<endl;
return 0;
}


uint16_t checkSum(void *buffer , size_t len)
{
	const uint16_t * buff=(uint16_t*) buffer;
	
	uint32_t sum;
	sum=0;
	while(len>1)
	{
		sum+=*buff++;
		if(sum & 0x80000000)
		{
			sum = (sum & 0xFFFF) + (sum >> 16);
		}
		len-=2;
	}
	if(len & 1)
	{
		sum+=*((uint8_t *)buff);
	}
	while(sum>>16)
	{
		sum=(sum & 0xFFFF)+(sum>>16);
	}
	return ((uint16_t)(~sum));
}
