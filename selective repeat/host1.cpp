//selective repeat
#include<iostream>
#include<fstream>
#include<time.h>
#include<unistd.h>
#include<cstring>
#include<string.h>
#include<cstdlib>
#include<sys/wait.h>
#include<semaphore.h>
#include<pthread.h>
# include<unistd.h>
#include<wait.h>
#include"unp.h"
using namespace std;

#define MYPORT 1235

pthread_mutex_t control;

//////
int sockfd,i=0;
const int windowSize=8;
int sendBase=0;
int seq_no=0;
int sent,received=0;
int total_packets;
char *ack=new char[100];
char ackBuffer[windowSize];
/////
struct rdt_pack
{
    char packet[11];
    int seq;
    uint16_t check_sum;
};

struct packetBuffer
{
    struct rdt_pack packet;
    bool isSent=false;
    bool isAcked=false;
};

uint16_t checkSum(void *buffer , size_t len);
void PreparePackets();
void InitAckBuffer();
int send_packets();
int rdt_send(int);
void*SendPacketNow(void*);
void NewPacketThread(int index);

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

struct packetBuffer PacketBuffer[200];// here we have to declare it to windowSize*2
    socklen_t length;
    struct sockaddr_in servr_addr,myaddr2;
    ifstream fin("input.txt");

    int main()
    {
        pthread_mutex_init(&control,NULL);
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	struct timeval timeout;      
    timeout.tv_sec = 4;
    timeout.tv_usec = 0;

    if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,sizeof(timeout)) < 0)
    perror("setsockopt failed\n");
	servr_addr.sin_family = AF_INET;
	servr_addr.sin_port = htons(MYPORT); // short, network byte order
	servr_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	memset(&(servr_addr.sin_zero),'\0', 8); // zero the rest of the struct
	int b=sizeof(struct sockaddr);
	bind(sockfd,(struct sockaddr *)&servr_addr, b);
    perror("bind : ");
    length=sizeof(myaddr2);
int a=recvfrom(sockfd, ack, 18, 0,(struct sockaddr*)&myaddr2, &length);
    perror("recvfrom status: ");
    cout<<ack<<endl;
        PreparePackets();
        while(received<total_packets)
        {
            send_packets();
        }
    int s=close(sockfd);
    return 0;
}

int send_packets()
{
    sleep(1);
        for(int i=0;i<windowSize;i++)
        {
            if(sent<windowSize)
            {
                if(!PacketBuffer[sendBase+i].isSent)// send a new packet
                {
                    PacketBuffer[sendBase+i].packet.seq=seq_no;
                    pthread_mutex_lock(&control);
                    sent++;
                    if(sent==windowSize)
                        sent=0;
                    seq_no++;
                    pthread_mutex_unlock(&control);

                    cout<<"----------------------------"<<endl;
                    //cout<<"seqNo :"<<seq_no<<endl;
                    cout<<"data :"<<PacketBuffer[sendBase+i].packet.packet<<endl;
                    //cout<<"check_sum :"<<PacketBuffer[seq_no].packet.check_sum<<endl;
                    cout<<"seq no inside :"<<PacketBuffer[sendBase+i].packet.seq<<endl;
                    cout<<"----------------------------"<<endl;
                    NewPacketThread(sendBase+i);
                }
            }
        }
    return 0;
}
void NewPacketThread(int index)
{
    sleep(2);
    pthread_t thread;
    pthread_create(&thread,NULL,SendPacketNow,&index);//check here
}

int rdt_send(int index)
{
    PacketBuffer[index].isSent=true;
    int z=sendto(sockfd, &PacketBuffer[index], sizeof(rdt_pack), 0,(struct sockaddr*)&myaddr2,length);
    return 0;
}

void* SendPacketNow(void *b)
{

    pthread_detach(pthread_self());
    int index=*((int*)b);
    cout<<pthread_self()<<" with index "<<index<<endl;
    pthread_mutex_lock(&control);
    rdt_send(index);
    pthread_mutex_unlock(&control);

    rdt_pack ack;
    while(1)
    {
        int a=recvfrom(sockfd, &ack,sizeof(rdt_pack),0,(struct sockaddr*)&myaddr2, &length);
        cout<<ack.seq<<" instead of "<<index<<endl;
        if(a==0)
        {
            PacketBuffer[index].isAcked="false";
            cout<<"..resending.."<<endl;
            int z=sendto(sockfd, &PacketBuffer[index], sizeof(rdt_pack), 0,(struct sockaddr*)&myaddr2,length);
            continue;
        }
        if(a<0)
        {
            // unhandled here
            pthread_exit(0);
        }
        if(ack.seq>=sendBase && ack.seq<(sendBase+windowSize))//if ack is in window range
        {
            for(int i=0;i<windowSize;i++)
            {
                if(ack.seq==i+sendBase)//checks here
                {
                    pthread_mutex_lock(&control);
                        sent--;
                        received++;
                    pthread_mutex_unlock(&control);
                        cout<<"received ack is "<<ack.seq<<endl;
                        PacketBuffer[ack.seq].isAcked=true;
                        if(ack.seq==sendBase)//changed from 0 to..
                        {
                            for(int j=0;j<windowSize;j++)
                            {
                                if(!PacketBuffer[j+sendBase].isAcked)
                                {
                                    pthread_mutex_lock(&control);
                                    sendBase=sendBase+j;
                                    pthread_mutex_unlock(&control);
                                    cout<<"|>base moved to "<<sendBase<<endl;
                                    break; //break j
                                }
                            }
                        }
                        break; //break i
                }
            }
            break; // break while
        }
    }
  pthread_exit(NULL);
}
uint16_t checkSum(void *buffer , size_t len)
{
	const uint16_t * buff=(uint16_t*) buffer;
	
	uint32_t sum;
	sum=0;
	while(len>1)
	{
		sum+=*buff++;
		if(sum & 0x80000000)
		{
			sum = (sum & 0xFFFF) + (sum >> 16);
		}
		len-=2;
	}
	if(len & 1)
	{
		sum+=*((uint8_t *)buff);
	}
	while(sum>>16)
	{
		sum=(sum & 0xFFFF)+(sum>>16);
	}
	return ((uint16_t)(~sum));
}

void InitAckBuffer()
{
    for (int i=0;i<windowSize;i++)
    {
        ackBuffer[i]=char(48+i);
        cout<<ackBuffer[i]<<endl;
    }
}

void PreparePackets()
{
    char buf[11];
    int i=0;
    while(!fin.eof())
    {
        fin.read(buf,10);
        buf[10]='\0';
        rdt_pack newPacket;
        newPacket.check_sum=checkSum(buf,strlen(buf));
        strcpy(newPacket.packet,buf);
        newPacket.seq=i;
        PacketBuffer[i].packet=newPacket;
        i++;
    }
    total_packets=i;
}
