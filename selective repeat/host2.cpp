#include<iostream>
#include<fstream>
#include"unp.h"
using namespace std;
#define MYPORT 1235
////////////////////
struct rdt_pack
{
    char packet[11];
    int seq;
    uint16_t check_sum;
};

struct rdt_pack rdt_recv();

int NextUnAckedPacket();
uint16_t checkSum(void *buffer , size_t len);
//////////////////
const int windowSize=8;
int nextSeqNo,recvBase=0;
rdt_pack ack;
socklen_t length;
ofstream fout("output.txt");

struct sockaddr_in cl_addr,myaddr2;
int sockfd;
////////////////////
struct packetBuffer
{
    struct rdt_pack packet;
    bool isAcked=false;
};

struct packetBuffer Buffer[windowSize];
///////////////
int counter=0;
int main()
{
    sockfd=socket(AF_INET, SOCK_DGRAM, 0);
    cl_addr.sin_family = AF_INET;
    cl_addr.sin_port = htons(MYPORT); // short, network byte order
    cl_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    socklen_t tolen=sizeof(cl_addr);
    char msg[]="hi lets start rdt\0";
    int s=sendto(sockfd, msg, strlen(msg), 0,(struct sockaddr*)&cl_addr,tolen);
    perror("sendto status: ");
    struct rdt_pack Ack;
    length=sizeof(myaddr2);
    //int i=0;

    while(1)
    {
      Ack=rdt_recv();
        if(strcmp(Ack.packet,"end")!=0)//ack.packet
        {
            cout<<"sending ack = "<<Ack.seq<<endl;
            s=sendto(sockfd, &Ack, sizeof(rdt_pack), 0,(struct sockaddr*)&myaddr2,tolen);////#### tolen
        }

        else
            break;
    }
    int k=close(sockfd);
    return 0;
}

struct rdt_pack rdt_recv()
{
    struct rdt_pack rdt_packet;
    int a=recvfrom(sockfd, &rdt_packet , sizeof(rdt_pack), 0,(struct sockaddr*)&myaddr2, &length);
    if(a<0)
    {
        cout<<"not handled"<<endl;
    }
    else
    {
        int s=checkSum(rdt_packet.packet,strlen(rdt_packet.packet));
        if(strcmp(rdt_packet.packet,"end")!=0)
    {
        //cout<<"---------Packet Info------------"<<endl;
        //cout<<"nextSeqNo :"<<nextSeqNo<<endl;
        cout<<"data :"<<rdt_packet.packet<<endl;
        //cout<<"check_sum :"<<rdt_packet.check_sum<<endl;
        cout<<"seq no inside :"<<rdt_packet.seq<<endl;
        cout<<"_____________________________"<<endl;

        // if packet is not corrupt and seqno is in window range
        if(s==rdt_packet.check_sum && (rdt_packet.seq>=recvBase && rdt_packet.seq<(windowSize+recvBase)))
        {
            nextSeqNo++;
            Buffer[rdt_packet.seq].isAcked=true;
            //cout<<"--condition met--"<<endl;
            for(int i=0;i<windowSize;i++)
            {
                if(i+recvBase==rdt_packet.seq)
                {
                    strcpy(Buffer[rdt_packet.seq].packet.packet,rdt_packet.packet);//data received in buffer
                    if(i==0)//if ack is of recvBase, slide window (after get/setting data)
                    {
                        recvBase=NextUnAckedPacket();
                        ack.seq=rdt_packet.seq;
                        cout<<"--base moved to "<<recvBase<<endl;
                        return ack;
                    }
                    else
                    {
                       // cout<<"--not base packet--"<<endl;
                        ack.seq=recvBase+i;
                    }
                    return  ack;
                }
            }
        }

        if(s!=rdt_packet.check_sum)
        {
            // check here again
            ack.seq=recvBase-1;
            return ack;
        }
    }// !=end

    else
    {
        ack.seq=-2;
        strcpy(ack.packet,"end");
        return ack;
    }
    }
}

int NextUnAckedPacket()
{
    for(int i=0;i<windowSize;i++)
    {
        if(Buffer[i].isAcked)
        {
            cout<<"data writting to file"<<endl;
            fout<<Buffer[i].packet.packet;
            if(i==windowSize-1)// check here
            {recvBase=nextSeqNo;
                return recvBase;}
        }
        else
        {
            recvBase=i;
            return recvBase;
        }
    }
}



/////////////////////////////////////////////
uint16_t checkSum(void *buffer , size_t len)
{
    const uint16_t * buff=(uint16_t*) buffer;

    uint32_t sum;
    sum=0;
    while(len>1)
    {
        sum+=*buff++;
        if(sum & 0x80000000)
        {
            sum = (sum & 0xFFFF) + (sum >> 16);
        }
        len-=2;
    }
    if(len & 1)
    {
        sum+=*((uint8_t *)buff);
    }
    while(sum>>16)
    {
        sum=(sum & 0xFFFF)+(sum>>16);
    }
    return ((uint16_t)(~sum));
}
